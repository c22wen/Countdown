function updateCounter(startDay){
	var time = new Date() - startDay;
	return{
		'days': Math.floor(time/(1000*60*60*24)),
		'hours': Math.floor((time/(1000*60*60))%24),
		'minutes': Math.floor((time/1000/60)%60),
		'seconds': Math.floor((time/1000)%60),
		'total': time
	};
}

function animateClock(span){
	span.className = "turn";
	setTimeout(function(){
		span.className = "";
	}, 700);
}

function startCounter(id, startDay){
	var timerInterval = setInterval(function(){ // function, every x ms
		var clock = document.getElementById(id);
		var timer = updateCounter(startDay);

		clock.innerHTML = '<span>' + timer.days + '</span>'
						+ '<span>' + timer.hours + '</span>'
						+ '<span>' + timer.minutes + '</span>'
						+ '<span>' + timer.seconds + '</span>';
		var spans = clock.getElementsByTagName("span");
		animateClock(spans[3]);
		if(timer.seconds == 59) animateClock(spans[2]);
		if(timer.minutes == 59 && timer.seconds == 59) animateClock(spans[1]);
		if(timer.hours == 23 && timer.minutes == 59 && timer.seconds == 59) animateClock(spans[0]);
	

	}, 1000);
}

window.onload = function(){
	var startDay = new Date("November 25, 2018 15:00:00");
	startCounter("clock", startDay);
};
